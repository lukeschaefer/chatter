import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;

import javax.swing.ButtonGroup;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.KeyStroke;


public class MenuBar extends JMenuBar{
	//Where the GUI is created:
	JMenu menu, submenu;
	JMenuItem connect, settings;
	JRadioButtonMenuItem rbMenuItem;
	JCheckBoxMenuItem cbMenuItem;

	public MenuBar(){
	super();
	

	//Build the first menu.
	menu = new JMenu("Settings");
	menu.setMnemonic(KeyEvent.VK_A);
	menu.getAccessibleContext().setAccessibleDescription(
	        "The only menu in this program that has menu items");
	add(menu);

	//a group of JMenuItems
	connect = new JMenuItem("Connect too...",
	                         KeyEvent.VK_T);
	connect.setAccelerator(KeyStroke.getKeyStroke(
	        KeyEvent.VK_1, ActionEvent.ALT_MASK));
	connect.getAccessibleContext().setAccessibleDescription(
	        "This doesn't really do anything");
	menu.add(connect);

	settings = new JMenuItem("Set username");
	settings.setMnemonic(KeyEvent.VK_B);
	menu.add(settings);

	//Build second menu in the menu bar.
	menu = new JMenu("Another Menu");
	menu.setMnemonic(KeyEvent.VK_N);
	menu.getAccessibleContext().setAccessibleDescription(
	        "This menu does nothing");
	add(menu);
	}
	
	public void setListener(ActionListener listener){
		connect.addActionListener(listener);
		settings.addActionListener(listener);
	}
}
