import java.awt.Color;
import java.io.Serializable;

import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;


public class Message implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	ChatUser from;
	ChatUser to;
	String text;
	boolean fromServer;

	public Message(ChatUser from, String text, boolean fromServer){
		this.from = from;
		this.text = text;
		this.fromServer = fromServer;

	}
	public Message(ChatUser from, ChatUser to, String text, boolean fromServer){
		this.from = from;
		this.text = text;
		this.to = to;
		this.fromServer = fromServer;

	}
	public Message(String text2) {
		this.text =text2;
	}
}
