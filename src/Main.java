import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;

import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;

public class Main implements ActionListener{
	static Main main;
	private MainFrame window;
	private ClientConnection connection;
	private ArrayList<ChatUser> userList;
	 SimpleAttributeSet textStyle;

	public static void main(String[] args){
		main = new Main();
	}
	
	public Main(){
		window = new MainFrame();
		window.setListener(this);
		userList = new ArrayList<ChatUser>();
		textStyle = new SimpleAttributeSet();
		textStyle.addAttribute(StyleConstants.CharacterConstants.Bold, Boolean.TRUE);
		connection = new ClientConnection(this);
		}

	public void connectTo() throws UnknownHostException, IOException{
			if(connection.connected)
				connection.disconnect();
			connection.connect("127.0.0.1", 9002);		
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		
		// IF CHAT MESSAGE
		if(e.getSource() == window.chatInput || e.getSource() == window.chatSend){
			if(!window.chatInput.getText().equals("")){
				connection.sendMessage(window.chatInput.getText());
				window.chatInput.setText("");
			}
		}
		
		// IF PM
		if(e.getSource() == window.pmInput || e.getSource() == window.pmSend){
			if(window.pmInput.getText().length()>0){
				connection.pmMessage(window.users.getSelectedValue(), window.pmInput.getText());
				window.pmInput.setText("");				
			}
		}
		
		// IF 'CONNECT TO'
		if(e.getSource() == window.menuBar.connect){
			System.out.println("Opening Connect-To panel.");
			ConnectPanel cp = new ConnectPanel(this);
		}
		
	}

	public void gotMessage(Message message) {
		System.out.println("Client got message "+message);
		try
		{
		   if(!message.fromServer){
			   setTextColor(message.from.getColor());
			   window.chatPanel.insertString(window.chatPanel.getLength(), message.from.getName()+":", textStyle );
		   }
		   else{
			   setTextColor(0);
		 	   window.chatPanel.insertString(window.chatPanel.getLength(), "Server:", textStyle );
		   }
		   window.chatPanel.insertString(window.chatPanel.getLength(), "  "+message.text+"\n", null );
		   window.textScroller.getVerticalScrollBar().setValue(99999);
		}catch(Exception e)
		{
			System.err.println("error printing message.");
		}
	}

	public void updateUserList(ChatUser[] chatList) {

		System.out.println("Updating user list with users:");		
		for(ChatUser user : chatList)
			System.out.println("\t"+user.getName()+", "+user.getIp());
		
		window.users.setListData(chatList);
		
	}
	
	private void setTextColor(int color){
		textStyle.addAttribute(StyleConstants.ColorConstants.Foreground, Color.black);
		switch(color){
			case 0 : textStyle.addAttribute(StyleConstants.ColorConstants.Foreground, Color.black);
			break;
			case 1 : textStyle.addAttribute(StyleConstants.ColorConstants.Foreground, new Color(170,10,10));
			break;
			case 2 : textStyle.addAttribute(StyleConstants.ColorConstants.Foreground, new Color(150,80,0));
			break;
			case 3 : textStyle.addAttribute(StyleConstants.ColorConstants.Foreground, new Color(150,150,0));
			break;
			case 4 : textStyle.addAttribute(StyleConstants.ColorConstants.Foreground, new Color(0,170,10));
			break;
			case 5 : textStyle.addAttribute(StyleConstants.ColorConstants.Foreground, new Color(0,170,170));
			break;
			case 6 : textStyle.addAttribute(StyleConstants.ColorConstants.Foreground, new Color(0,10,170));
			break;
			case 7 : textStyle.addAttribute(StyleConstants.ColorConstants.Foreground, new Color(100,0,150));
			break;
			case 8 : textStyle.addAttribute(StyleConstants.ColorConstants.Foreground, new Color(100,100,100));
			break;
		}
	}

	public void gotPM(Message message) {
	 	   //window.pmPanel.insertString(window.chatPanel.getLength(), "Server:", textStyle );		
	}
	

}
