import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;

public class ClientConnection {
	Main listener;
	PrintWriter output;
	ObjectOutputStream objOut;
	GetMessageThread thread;
	Socket s;
	boolean connected;
	
	
	public ClientConnection(Main listener){	
		this.listener = listener;	
		connected = false;
	}
	
	public void sendMessage(String text) {
		try {
			objOut.writeObject(new Message(text));
			objOut.reset();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void pmMessage(ChatUser pmUser, String text) {
		
		try {
			objOut.writeObject(new PrivateMessage(pmUser, text));
			objOut.reset();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void disconnect() {
		output.close();
		thread.close();
		try {
			s.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		connected = false;
	}

	public void connect(String ip, int port) throws UnknownHostException, IOException {		
		s = new Socket(ip, port);	
		output = new PrintWriter(s.getOutputStream(), true);
		objOut = new ObjectOutputStream(s.getOutputStream());
		thread = new GetMessageThread(s.getInputStream(), listener);	
		connected = true;
	}
}
	

class GetMessageThread extends Thread{
	
	ObjectInputStream in;
	Main listener;
	private boolean quit = false;
	GetMessageThread(InputStream inStream, Main listener){
		
		this.listener = listener;
		try {
			this.in = new ObjectInputStream(inStream);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		start();
	}
	


	public void close() {
		try {
			in.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.quit  = true;
		
	}



	public void run(){
		while(!quit){
			try {
				Object object = in.readObject();
				if(object instanceof PrivateMessage){
					Message message = (Message) object;
					listener.gotPM(message);
				}
				else if(object instanceof Message){
					Message message = (Message) object;
					listener.gotMessage(message);
				}					
				else if(object instanceof ArrayList<?>){
					ChatUser[] chatList = new ChatUser[((ArrayList<ChatUser>)object).size()];
					((ArrayList<ChatUser>)object).toArray(chatList);
					listener.updateUserList(chatList);
				}
			} catch (IOException | ClassNotFoundException e) {
			}
			try {
				this.sleep(50);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	}
	}
}







