import java.awt.Button;
import java.awt.Container;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Label;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;


public class ConnectPanel extends JFrame implements ActionListener{
	Main listener;
	JLabel error;
	TextField ip;
	TextField port;
	
	public ConnectPanel(Main listener){
		
		this.listener = listener;
		
		this.setVisible(true);
		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		
		// Keep focus
		addWindowFocusListener(new WindowAdapter() {
			public void windowLostFocus(WindowEvent evt) {
			requestFocus();
			}
			});
		
		Container contentPanel = this.getContentPane();
		contentPanel.setLayout(new GridBagLayout());
		GridBagConstraints gbc = new GridBagConstraints();
		gbc.insets= new Insets(1,3,1,3);
		gbc.gridwidth = 2;		
	
		contentPanel.add(new Label("IP Address:"), gbc);
		
		gbc.gridwidth = 1;	
		gbc.gridx = 2;
		contentPanel.add(new Label("Port:"), gbc);
		
		gbc.gridwidth = 2;	
		gbc.gridx = 0;
		gbc.gridy = 1;
		ip = new TextField("127.0.0.1");
		contentPanel.add(ip, gbc);
		
		gbc.gridwidth = 1;	
		gbc.gridx = 2;
		gbc.gridy = 1;
		port = new TextField("9002");
		contentPanel.add(port, gbc);
		
		gbc.gridwidth = 1;	
		gbc.gridx = 0;
		gbc.gridy = 2;
		
		Button cancel = new Button("Cancel");
		cancel.addActionListener(this);
		
		contentPanel.add(cancel, gbc);
		
		gbc.gridwidth = 2;	
		gbc.gridx = 2;
		gbc.gridy = 2;
		
		Button connect = new Button("Connect");
		connect.addActionListener(this);
		
		contentPanel.add(connect, gbc);
		
		gbc.gridwidth = 3;
		gbc.gridheight = 5;
		gbc.gridx = 0;
		gbc.gridy = 3;
		error = new JLabel("<html><body>        <br>        <br>        </body></html>");
		
		contentPanel.add(error, gbc);
		
		this.pack();
		this.setResizable(false);
		this.setLocation(200,200);

	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// For some reason I can't figure out - this line doesn't work:
		//error.setText("<html><body style = \"color: green\"><br>Connecting...</body></html>");
		String label = ((Button)e.getSource()).getLabel();
		
		if(label == "Connect"){
			
			boolean worked = true;
			try {
				// Test to see if ip is valid:		
				
				if(ip.getText().matches("(\\d{1,3}+.){3}\\d+"))
					listener.connectTo();				
				else{
					worked = false;
					error.setText("<html><body style = \"color: red\"><br>Invalid IP Address</html></body>");
				}
			}catch(NumberFormatException e1){
				worked = false;
				error.setText("<html><body style = \"color: red\"><br>Invalid port</html></body>");
			}catch (IOException e1) {
				worked = false;				
				error.setText("<html><body style = \"color: red\">Could not connect to <br>"+ip.getText()+":"+port.getText()+"</body></html>");
			}
			
			if(worked)
				this.dispose();
		}
		if(label == "Cancel"){
			this.dispose();
		}
		
	}
}
