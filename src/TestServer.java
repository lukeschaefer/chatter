import java.io.*;
import java.net.*;
import java.util.ArrayList;


public class TestServer{
	private int port;
	private ServerSocket s;
	private ArrayList<ChatThread> threadList;
	public ArrayList<ChatUser> userList;
	
	TestServer(int port){
		this.port = port;
		
		try {
			s =  new ServerSocket(port);
		} catch (IOException e) {
			e.printStackTrace();
		}
		threadList = new ArrayList<ChatThread>();
		userList = new ArrayList<ChatUser>();
	}

	public ArrayList<ChatThread> getThreads(){
		return threadList;
	}
	void startChat() throws Exception{	
		System.out.println("Starting server with IP: "+InetAddress.getLocalHost().getHostAddress()+":"+port);
		while(true){
			Socket incoming = s.accept();								
			ChatThread chat = new ChatThread(incoming, this);	
			threadList.add(chat);
			chat.start();
			
		}
	}
	
	void tellClients(ChatUser from, String text){
		for(ChatThread chat : threadList)
			chat.tellClient(from, text);
	}
	
	public void updateUserLists() {
		for(ChatThread chat : threadList)
			chat.updateUserList();		
	}
	
	public void tellClients(String text) {
		for(ChatThread chat : threadList)
			chat.tellClient(text);
	}
	
	public static void main(String[] args){
		TestServer server = new TestServer(9002);	
		try {
			server.startChat();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	public void removeFromClients(ChatThread chatThread) {
		userList.remove(chatThread.thisUser);		
		threadList.remove(chatThread);
		updateUserLists();
	}

	public void pmClient(PrivateMessage pm) {
		for(ChatThread chat : threadList)
			if(chat.thisUser.getIp() == pm.to.getIp())
				chat.pmClient(pm);
		
	}



	
}
