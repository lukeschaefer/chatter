import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontFormatException;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.RenderingHints.Key;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.swing.JLabel;
import javax.swing.JPanel;


public class TitlePanel extends JPanel implements MouseListener{
	String title;
	Font onRamp;
	boolean hover = false;
	public TitlePanel(String title){
		this.title = title;
		this.setPreferredSize(new Dimension(40,30));
		this.addMouseListener(this);
		try {
			 
			InputStream is = this.getClass().getResourceAsStream("franchise.ttf");
			onRamp = Font.createFont(Font.TRUETYPE_FONT, is).deriveFont (Font.TRUETYPE_FONT, 18f);

		} catch (Exception e) {
			System.out.print("FONT ERROR!");
			e.printStackTrace();
		}
		this.setLayout(new BorderLayout());

		this.setBackground(new Color(100,100,100));

	}
	
	   @Override
	    protected void paintComponent(Graphics g) {
	        super.paintComponent(g);
	        g.setColor(new Color(80,80,80));
	        if(this.hover)
	        	g.setColor(new Color(190,190,190));
	        else
	        	g.setColor(new Color(170,170,170));
	        g.setFont(onRamp);
	        Graphics2D g2d = (Graphics2D)g;
	        g2d.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING,
                    RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
	        g2d.drawString(title, 5, 20);
	        
	  
	        
	        //g.fillRect(0, 0, 100, 100);
	    }

	@Override
	public void mouseClicked(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseEntered(MouseEvent arg0) {
		hover = true;

		repaint();
	}

	@Override
	public void mouseExited(MouseEvent arg0) {
		hover = false;

		repaint();
	}

	@Override
	public void mousePressed(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseReleased(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

}
