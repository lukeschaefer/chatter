import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Label;
import java.awt.Panel;
import java.awt.TextArea;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;


public class MainFrame{
	JFrame frame;
	JTextPane chatTextPane;
	JList<ChatUser> users;
	JTextField chatInput;
	JButton chatSend;
	JTextField pmInput;
	JButton pmSend;
	StyledDocument chatPanel;
	JTextPane pmTextPane;
	JScrollPane textScroller;
	MenuBar menuBar;
		public MainFrame(){
			//1. Create the frame.
			frame = new JFrame("Java Chat Client Test");
			//2. Optional: What happens when the frame closes?
			frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

			// Set menu bar;
			menuBar = new MenuBar();
			frame.setJMenuBar(menuBar);
			
			//3. Create components and put them in the frame.
			//...create emptyLabel...

			TitlePanel chatTitle = new TitlePanel("Chat Panel");
			chatTextPane = new JTextPane();
					
			TitlePanel userListTitle = new TitlePanel("User List");
			users = new JList<ChatUser>();
			frame.setSize(700,800);
			chatInput = new JTextField("");
			chatSend = new JButton("SEND");
			chatSend.setBorder(null);
			chatSend.setBackground(Color.white);
			
			pmInput = new JTextField("");
			pmSend = new JButton("SEND");
			pmSend.setBorder(null);
			pmSend.setBackground(Color.white);
			
			
			TitlePanel pmTitle = new TitlePanel("Private Messages");
			pmTextPane = new JTextPane();
			
			chatTextPane.setEditable(false);
			chatTextPane.setBackground(new Color(225,230,230));

			JPanel contentFrame = new JPanel(new GridBagLayout());
			GridBagConstraints gbc = new GridBagConstraints();
			GridBagConstraints gb = new GridBagConstraints();
			
			
			gbc.insets = new Insets(8,8,0,4);
			
			// CHAT AREA
				gbc.gridx = 0;
				gbc.gridy = 0;
				gbc.weighty = 1.0;			   
				gbc.weightx = 1.4;
				gbc.gridwidth = 2;
				gbc.gridheight = 2;
				gbc.weightx = 2;
				gbc.fill = GridBagConstraints.BOTH;
	
				JPanel chatHolder = new JPanel(new BorderLayout());
					
					textScroller = new JScrollPane(chatTextPane, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);  
					textScroller.setAutoscrolls(true);
					textScroller.setPreferredSize(new Dimension(150,100));
					textScroller.setBorder(null);
					chatHolder.add(chatTitle, BorderLayout.NORTH);	
					
					//chatHolder.add(chatTextPane, BorderLayout.CENTER);
					chatHolder.add(textScroller, BorderLayout.CENTER);
					chatHolder.addMouseListener(chatTitle);
				contentFrame.add(chatHolder, gbc);
				chatHolder.setBackground(Color.pink);
			
			// USER LIST AREA
				gbc.insets = new Insets(8,4,0,8);
				gbc.gridwidth = 2;
				gbc.gridheight = 1;
				gbc.gridx = 2;
				gbc.weightx = 1;
				gbc.gridy = 0;
				
				JPanel usersHolder = new JPanel(new BorderLayout());
					users.setBackground(new Color(230,230,225));
					usersHolder.add(userListTitle, BorderLayout.NORTH);
					usersHolder.add(users, BorderLayout.CENTER);
				contentFrame.add(usersHolder, gbc);
			
			// PM AREA
				gbc.gridy = 1;
				JPanel pmHolder = new JPanel(new BorderLayout());
					pmHolder.add(pmTitle, BorderLayout.NORTH);
					pmHolder.add(pmTextPane, BorderLayout.CENTER);
					pmTextPane.setBackground(new Color(230,225,230));
				contentFrame.add(pmHolder, gbc);
					
				
			// CHAT INPUT AREA
				gbc.insets = new Insets(1,8,1,4);
				gbc.gridx = 0;
				gbc.weighty = 0;
				gbc.gridwidth = 1;
				gbc.gridheight = 1;
				gbc.gridy = 2;
				gbc.fill = GridBagConstraints.HORIZONTAL;
				gbc.anchor = GridBagConstraints.NORTHWEST;
				JPanel chatInputHolder = new JPanel(new GridBagLayout());
				contentFrame.add(chatInputHolder, gbc);				
					gbc.fill = GridBagConstraints.BOTH;
					gbc.insets = new Insets(1,1,1,1);
					gbc.gridx = 0;
					gbc.gridy = 0;
					gbc.gridwidth = 4;
					gbc.weightx = 3;
					chatInputHolder.add(chatInput, gbc);
					gbc.gridx = 5;
					gbc.gridy = 0;
					gbc.gridwidth = 1;
					gbc.weightx = 0;
					chatInputHolder.add(chatSend, gbc);
			
			// PM Input Area
					gbc.insets = new Insets(1,4,1,8);
				gbc.gridx = 2;
				gbc.weighty = 0;
				gbc.weightx = 1;
				gbc.gridwidth = 1;
				gbc.gridheight = 1;
				gbc.gridy = 2;
				gbc.fill = GridBagConstraints.HORIZONTAL;
					JPanel pmInputHolder = new JPanel(new GridBagLayout());
					contentFrame.add(pmInputHolder, gbc);
					gbc.insets = new Insets(1,1,1,1);
					gbc.fill = GridBagConstraints.BOTH;
					gbc.gridx = 0;
					gbc.gridy = 0;
					gbc.gridwidth = 4;
					gbc.weightx = 3;
					pmInputHolder.add(pmInput, gbc);
					gbc.gridx = 5;
					gbc.gridy = 0;
					gbc.gridwidth = 1;
					gbc.weightx = 0;
					pmInputHolder.add(pmSend, gbc);
			
				
			contentFrame.setBackground(Color.white);
			frame.getContentPane().add(contentFrame);
		    chatPanel = chatTextPane.getStyledDocument();
		  
	
			frame.setVisible(true);
			
		}
		
	
	

		public void setListener(Main main) {
			chatInput.addActionListener(main);
			chatSend.addActionListener(main);
			pmInput.addActionListener(main);
			pmSend.addActionListener(main);
			menuBar.setListener(main);
			
		}





}
