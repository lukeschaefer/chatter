import java.awt.Color;
import java.net.InetAddress;


public class ChatUser implements java.io.Serializable{
	private int color;
	private String name;
	private InetAddress ip;
	private int id;
	
	ChatUser(InetAddress inetAddress, int id){
		setColor(0);
		setName(inetAddress.getHostName());
		setIp(inetAddress);
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getColor() {
		return color;
	}

	public void setColor(int color) {
		this.color = color;
	}

	public InetAddress getIp() {
		return ip;
	}

	public void setIp(InetAddress ip) {
		this.ip = ip;
	}
	
	public String toString(){
		return name;
	}
}
