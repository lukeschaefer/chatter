import java.io.IOException;
import java.net.Socket;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.io.*;

public class ChatThread extends Thread{
	ObjectInputStream in;
	ObjectOutputStream objOut;
	Socket incoming;
	TestServer server;
	ChatUser thisUser;
	
	public ChatThread(Socket incoming, TestServer server) throws IOException {
		super();
		this.incoming = incoming;	
		this.in = new ObjectInputStream(incoming.getInputStream());
		this.objOut = new ObjectOutputStream(incoming.getOutputStream());
		this.server = server;
		
	}

	public void start(){		
		tellClient("Welcome to the chat program, to see a list of commands, type \\cmd.");
		System.out.println("Received connection from "+incoming.getInetAddress().getHostAddress());
		thisUser = new ChatUser(incoming.getInetAddress(),2);
		server.tellClients(thisUser.getName()+" joined chat.");
		server.userList.add(thisUser);
		server.updateUserLists();
		super.start();
	}
	
	public void tellClient(String text){
		Message message = new Message(null,text,true);
		try {			
			objOut.writeObject(message);
			objOut.reset();
			objOut.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}		
	}
	
	public void tellClient(ChatUser from, String text){
		Message message = new Message(from,text,false);
		try {
			objOut.writeObject(message);
			objOut.flush();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
	}
	
	void updateUserList(){	
		try {
			objOut.writeObject(server.userList);
			objOut.reset();
			objOut.flush();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
	}
	
	public void run(){		
		
		boolean done = false;
		while(!done){
			System.out.println("Searching for a message!");
			try {
				Object object = in.readObject();	
				if(object instanceof PrivateMessage){
					PrivateMessage pm = (PrivateMessage) object;
					String str = pm.message;	
					if(!str.trim().equals("")){		
						server.pmClient(pm);						
					}
				}
				else if(object instanceof Message){
					Message message = (Message) object;
					String str = message.text;		
					if(!str.trim().equals("")){						
						if(str.trim().charAt(0) == '\\')
							commandResponse(str);							
						else
							server.tellClients(thisUser,str);						
					}
				}
			} catch (IOException e) {
				done = true;				
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		System.out.println("Ended connection to: "+incoming.getInetAddress().getHostAddress());
		server.removeFromClients(this);
		server.tellClients(thisUser,thisUser.getName()+" exited the chat session.");

	}

	private void commandResponse(String str) {
		DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
		Date date = new Date();
		String cmd = str.toLowerCase().split(" ")[0].substring(1);
		System.out.println("Got command "+cmd);
		if(cmd.equals("time"))
			tellClient("Time is: "+dateFormat.format(date));
		if(cmd.equals("cmd"))
			tellClient("Commands are:\n\\time\n\\users\n\\cmd\n\\setName\n\\ips");
		if(cmd.equals("users"))
			for(ChatThread chat: server.getThreads())
				tellClient(""+thisUser.getName());
		if(cmd.equals("ips"))
			for(ChatThread chat: server.getThreads())
				tellClient(""+thisUser.getName()+" ("+chat.incoming.getInetAddress().getHostAddress()+")");
	
		if(cmd.equals("setname")){
			server.tellClients(""+thisUser.getName()+" changed name to "+str.split(" ")[1]);
			thisUser.setName(str.split(" ")[1]);
			server.updateUserLists();
		}	
		
		if(cmd.equals("setcolor")){
			if(str.toLowerCase().matches("\\\\setcolor \\d{1,2}")){
				thisUser.setColor(Integer.parseInt(str.split(" ")[1]));
				tellClient("Set color to "+thisUser.getColor());
			}
			else if(str.toLowerCase().matches("\\\\setcolor \\D{1,}")){
				String color = str.split(" ")[1];
				switch(color){
					case "black" : thisUser.setColor(0);
					break;
					case "red" : thisUser.setColor(1);
					break;
					case "orange" : thisUser.setColor(2);
					break;
					case "yellow" : thisUser.setColor(3);
					break;
					case "green" : thisUser.setColor(4);
					break;
					case "cyan" : thisUser.setColor(5);
					break;
					case "blue" : thisUser.setColor(6);
					break;
					case "purple" : thisUser.setColor(7);
					break;
					case "grey" : thisUser.setColor(8);
					break;
					case "gray" : thisUser.setColor(8);
					break;
					default : tellClient("Valid colors are black, blue, red, green, orange, pink, grey, purple, cyan and yellow.");
									
				}
				tellClient("Set color to "+color);
			}
			else
				tellClient("Proper use of \\setColor is:\n\t \"\\setColor [color name OR number 1-12]\"");
			//server.updateUserLists();
		}
		
		
	}

	public void pmClient(PrivateMessage pm) {
		try {
			objOut.writeObject(pm);
			objOut.flush();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}				
	}
	
	

}
