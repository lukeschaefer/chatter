import java.io.Serializable;


public class PrivateMessage implements Serializable{
	ChatUser to;
	String message;

	
	PrivateMessage(ChatUser to, String message){
		this.to = to;
		this.message = message;
	}

}
